
#include <stdio.h>
#include <math.h> // sqrt
#include <unistd.h>
#include <string.h>
#include <libscrypt.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#include "ntwrk.h"

/*
http://www.cs.unc.edu/~jeffay/dirt/FAQ/linuxBinaryTips.html
*/

const char* salt = "I Hate Liam Echlin";
const char* DOWNSTREAM = "10.0.48.151";
const char* DOWNSTREAM_PORT = "1111";
pthread_mutex_t airLock;
pthread_mutex_t chlLock;
pthread_mutex_t totLock;

int handle_selenium(struct node* payload, uint32_t* prevMoles, size_t jumps, size_t* numMoles);
int handle_phosphates(struct node* payload, uint32_t* pastMoles, size_t idx);
void printPayload(struct node* payload, size_t numMoles);
int incGlobals(int which, size_t howMuch);
int isPrime(uint32_t num);
int isFib(uint32_t data);
int isUndulating(uint32_t num);
int sludgy(uint32_t data);
int hazzy(uint32_t dataMole);
int updatePointers(struct node** payload, size_t* numMoles, uint32_t toRemove);
void* handler(void* inc_stream);
int sendPayload(int output, struct header* head, void* payload, size_t numMoles);

size_t totalMoles = 1;
size_t chlNum = 1;
size_t airNum = 1;

int incGlobals(int which, size_t howMuch)
{
	if(which == 1) {

		pthread_mutex_lock(&airLock);

		airNum+= howMuch;

		pthread_mutex_unlock(&airLock);
	}
	else if(which == 2) {
		pthread_mutex_lock(&chlLock);

		chlNum+= howMuch;

	 	pthread_mutex_unlock(&chlLock);
	}
	else if(which == 3) {
		pthread_mutex_lock(&totLock);

		totalMoles+= howMuch;

	 	pthread_mutex_unlock(&totLock);
	}
	else {
		return(-1);
	}

	return(0);

}


int isPrime(uint32_t num)
{
	if(num % 2 == 0) {
		return(0);
	}

	size_t tar = sqrt(num);

	for(size_t i = 3; i < tar; i+=2)
	{
		if(num % i == 0) {
			return(0);
		}
	}

	return(1);
}

int isUndulating(uint32_t num)
{
	// unsigned parameter so negatives will be large
	if(num <= 9) {
		// single numbers are undulating
		return(1);
	}
	char stringified[10] = {'\0'};
	int prev = 0;
	int temp = 0;
	// TODO: UNDULATING NUMBER!
	snprintf(stringified, sizeof(stringified),"%d", num);

	if(stringified[0] > stringified[1]) {
		prev = 1;
	}
	else if(stringified[0] < stringified[1]) {
		prev = -1;
	}
	else {
		return(0);
	}

	size_t tar = strlen(stringified) -1;
	for(size_t idx = 1; idx< tar; idx++) {
		if(stringified[idx] == '\0') {
			continue;
		}
		temp = stringified[idx] - stringified[idx+1];
		if(temp > 0) {
			temp = 1;
			if(prev == temp) {
				// not undulating
				return(0);
			}
			prev = temp;
		}
		else if(temp < 0) {
			temp = -1;
			if(prev == temp) {
				// not undulating
				return(0);
			}
			prev = temp;
		}
		else {
			return(0);
		}
	}
	return(1);
}

int sludgy(uint32_t data)
{
	size_t hashSz = 64;
	uint8_t* hashes = malloc(hashSz);
	memset(hashes,'\0', hashSz);
	struct header head = {0};

	char buf[16];
	snprintf(buf, sizeof(buf), "%u", data);
	printf("Sludging:'%s' aka 0x%0X\n", buf, data);
	libscrypt_scrypt((const uint8_t *)buf, strlen(buf),
			(const uint8_t *)salt, strlen(salt),
			2048, 4, 4,
			hashes, hashSz);
	head.size = htons(sizeof(head) + hashSz);
	head.type = htons(2);


	int osludge = connect_outgoing(DOWNSTREAM, "4444");
	if(osludge < 0) {
		perror("Could not connect downstream");
		free(hashes);
		return(-1);
	}

	sendPayload(osludge, &head, hashes, 8);

	// send(osludge, &head, sizeof(head), 0);
	// send(osludge, hashes, hashSz, 0);

	close(osludge);

	free(hashes);
	return(0);
}

int hazzy(uint32_t dataMole)
{
	if(dataMole == 0) {
		return(0);
	}
	struct haz_container haz = {0};
	struct header head = {0};
	haz.data = htonl(dataMole);
	printf("Hazmatting: 0x%0X htonl: 0x%0X\n", dataMole, haz.data);

	head.size = htons(sizeof(head) + sizeof(haz));
	head.type = htons(4);

	int ohaz = connect_outgoing(DOWNSTREAM, "8888");
	if(ohaz < 0) {
		perror("Could not connect downstream");
		return(-1);
	}

	sendPayload(ohaz, &head, &haz, 1);

	// send(ohaz, &head, sizeof(head), 0);
	// send(ohaz, &haz, sizeof(haz), 0);

	close(ohaz);

	return(0);
}

int updatePointers(struct node** payload, size_t* numMoles, uint32_t toRemove)
{
	printf("Updating pointers. num: %zu  removing: %u", *numMoles, toRemove);
	int toRemovePos = -1;
	for(size_t n = 0; n < *numMoles; n++)
	{
		if(((struct node*)*payload)[n].data == toRemove)
		{
			toRemovePos = n;
			if(((struct node*)*payload)[n].left == toRemovePos)
			{
				((struct node*)*payload)[n].left = 0;
			}
			if(((struct node*)*payload)[n].rite == toRemovePos)
			{
				((struct node*)*payload)[n].rite = 0;
			}
			break;
		}
	}
	if(toRemovePos == -1) {
		printf("Could not find molecule to remove.\n");
		return(-1);
	}
	printf("toRemove is: %d\n", toRemovePos);
	for(size_t i = 0; i < *numMoles; i++)
	{
		if(((struct node*)*payload)[i].left == toRemovePos) {
			((struct node*)*payload)[i].left = ((struct node*)*payload)[toRemovePos].left;
		}
		if(((struct node*)*payload)[i].rite == toRemovePos) {
			((struct node*)*payload)[i].rite = ((struct node*)*payload)[toRemovePos].rite;
		}
	}
	printf("Updated other molecules.\n");

	memmove(*payload+toRemovePos, *payload+(toRemovePos+1), (((*numMoles)-(toRemovePos+1))*8) );
	*numMoles-=1;
	printf("Memmoved.\n");
	*payload = realloc(*payload, (*numMoles)*sizeof(struct node));
	printf("Realloced.\n");

	for(size_t i = 0; i < *numMoles; i++)
	{
		if(((struct node*)*payload)[i].left > toRemovePos) {
			((struct node*)*payload)[i].left -= 1;
		}
		if(((struct node*)*payload)[i].rite > toRemovePos) {
			((struct node*)*payload)[i].rite -= 1;
		}
	}

	printf("Now have %zu molecules.\n", *numMoles);
	return(0);
}


void* handler(void* inc_stream)
{
	int new_stream = *(int*)inc_stream;
	struct header head;

	ssize_t received_bytes = read(new_stream, &head, sizeof(head));
	if(received_bytes < 0) {
		perror("Could not read header");
		close(new_stream);
		return(NULL);
	}
	if((size_t)received_bytes < sizeof(head)) {
		// TODO: Should log this and send a report packet
		fprintf(stderr, "Did not receive a full header (%zd/%zu)\n",
				received_bytes, sizeof(head));
		close(new_stream);
		return(NULL);
	}

	size_t bytes_to_read = ntohs(head.size) - sizeof(head);
	// Leave the last node as a nice all-0 node
	struct node *payload = calloc(bytes_to_read/8, sizeof(payload));
	received_bytes = 0;

	// Read in the entire payload
	do {
		ssize_t amt = read(new_stream, &((char *)payload)[received_bytes], bytes_to_read - received_bytes);
		if(amt < 0) {
			// TODO: Should log this and send a report packet
			fprintf(stderr, "Did not receive a entire packet\n");
			goto done;
		}
		received_bytes += amt;
	} while((size_t)received_bytes < bytes_to_read);

	size_t numMoles = bytes_to_read/8;
	printf("Now have %zu molecules.\n", numMoles);

	for(size_t n = 0; n < numMoles; n++)
	{
		payload[n].data = htonl(payload[n].data);
		payload[n].left = htons(payload[n].left);
		payload[n].rite = htons(payload[n].rite);
	}

	// Fungus/Bacteria cleaning
	printf("Fungus/Bacteria cleaning\n");
	for(size_t n = 0; n < numMoles; n++) {
		if(payload[n].data == 0) {
			updatePointers(&payload, &numMoles, payload[n].data);
			continue;
		}
		if(isFib(payload[n].data) == 1) {
			printf("Found fungus.\n");
			payload[n].rite = 0;
			payload[n].left = 0;
			hazzy(payload[n].data);
			updatePointers(&payload, &numMoles, payload[n].data);
		}
	}

	// Debris cleaning
	printf("Debris cleaning\n");
	int foundDeb = 0;
	for(size_t n = 0; n < numMoles; n++) {
		if(payload[n].data == 0) {
			updatePointers(&payload, &numMoles, payload[n].data);
			continue;
		}
		if(payload[n].rite > numMoles) {
			// Debris found!
			foundDeb = 1;
			payload[n].rite = 0xFFFF;
		}
		if(payload[n].left > numMoles) {
			// Debris found!
			foundDeb = 1;
			payload[n].left = 0xFFFF;
		}
	}
	if(foundDeb == 1)
	{
		printf("Found debris!\n");
		// Lead cleaning
		// Converted from python based on pirance's implementation
		printf("Lead cleaning before debris.\n");
		for(size_t n = 0; n < numMoles; n++) {
			if(isPrime(payload[n].data) == 1) {
				continue;
			}
			if(isUndulating(payload[n].data) == 1) {
				continue;
			}
			int num = (sqrt(1+8*payload[n].data - 1))/2;
			uint32_t total = (num*(num+1))/2;
			if(total == payload[n].data) {
				// lead found.
				printf("Found lead: %u.\n", payload[n].data);
				hazzy(payload[n].data);
				payload[n].data = 0;
				uint32_t tempLeft = payload[n].left;
				payload[n].left = payload[n].rite;
				payload[n].rite = tempLeft;
			}
		}
		int odeb = connect_outgoing(DOWNSTREAM, "2222");
		if(odeb < 0) {
			perror("Could not connect downstream");
			return(NULL);
		}

		head.size = sizeof(head)+(numMoles * sizeof(*payload));
		head.type = 1;
		head.size = htons(head.size);
		head.type = htons(head.type);
		for(size_t n = 0; n < numMoles; n++)
		{
			payload[n].data = htonl(payload[n].data);
			payload[n].left = htons(payload[n].left);
			payload[n].rite = htons(payload[n].rite);
		}

		sendPayload(odeb, &head, payload, numMoles);
		// send(odeb, &head, sizeof(head), 0);
		// send(odeb, payload, sizeof(*payload) * numMoles, 0);

		close(odeb);
		return(NULL);
	}

	// Lead cleaning
	// Converted from python based on pirance's implementation
	printf("Lead cleaning\n");
	for(size_t n = 0; n < numMoles; n++) {
		if(isPrime(payload[n].data) == 1) {
			continue;
		}
		if(isUndulating(payload[n].data) == 1) {
			continue;
		}
		int num = (sqrt(1+8*payload[n].data + 1))/2;
		uint32_t total = (num*(num+1))/2;
		if(total == payload[n].data) {
			// lead found.
			printf("Found lead: %u.\n", payload[n].data);
			hazzy(payload[n].data);
			payload[n].data = 0;
		}
	}

	printf("Mercury cleaning\n");
	uint32_t* indegrees = malloc(sizeof(*indegrees) * (numMoles+1));
	memset(indegrees, '\0', numMoles);
	for(size_t n = 0; n < numMoles; n++) {
		indegrees[payload[n].left] = 1;
		indegrees[payload[n].rite] = 1;
	}
	size_t numIndegrees = 0;
	for(size_t n = 0; n < numMoles; n++) {
		if(indegrees[n] == 0) {
			indegrees[numIndegrees] = n;
			numIndegrees++;
		}
	}
	while(numIndegrees > 1)
	{
		// nowhere to go but down
		uint32_t min = UINT32_MAX;
		for(size_t n = 0; n < numIndegrees; n++)
		{
			// printf("comparing: %u to %u\n", payload[indegrees[n]].data, min);
			if(payload[indegrees[n]].data < min)
			{
				min = payload[indegrees[n]].data;
			}
		}

		numIndegrees--;
		hazzy(min);
		updatePointers(&payload, &numMoles, min);
		printf("Removing mercury! mole#:%u\n", min);
	}
	free(indegrees);

	// Selenium cleaning
	printf("Selenium cleaning\n");
	size_t cur = 0;
	size_t tar = 0;
	size_t jumps = 0;
	uint32_t* prevMoles = malloc( sizeof(*prevMoles) * numMoles );

	for(size_t n = 0; n < numMoles; n++)
	{
		memset(prevMoles, '\0', numMoles);
		tar = payload[n].data;
		cur = n;
		jumps = 0;

		while(jumps < numMoles)
		{
			if(payload[cur].left == payload[cur].rite)
			{
				// printf("%zu -> ", cur);
				if(payload[cur].left == 0) {
					// not a selenium molecule
					// printf("End.\n");
					break;
				}
				prevMoles[jumps] = cur;//payload[cur].data;
				cur = payload[cur].left;
				if(payload[cur].data == tar)
				{
					// Selenium found!
					printf("Found selenium! mole#:%u\n", payload[n].data);
					handle_selenium(payload, prevMoles, jumps, &numMoles);
					break;
				}
			}
			else {
				// Not a selenium molecule
				// printf("End.\n");
				break;
			}

			jumps++;
		}
	}
	free(prevMoles);


	// Feces cleaning
	printf("Feces cleaning\n");
	for(size_t n = 0; n < numMoles; n++) {
		if(isPrime(payload[n].data)) {
			// Feces found!
			printf("Found feces! mole#:%u -- %u > %u\n", payload[n].data, payload[n].left, payload[n].rite);
			int retV = sludgy(payload[n].data);
			if(retV < 0) {
				printf("Fail to sludge.\n");
				return(NULL);
			}
			updatePointers(&payload, &numMoles, payload[n].data);
		}
	}

	// Ammonia cleaning
	printf("Ammonia cleaning\n");
	for(size_t n = 0; n < numMoles; n++)
	{
		if(payload[n].data == 0) {
			continue;
		}
		if( isUndulating(payload[n].data) ) {
			// Ammonia found!
			printf("Found ammonia! mole#:%u -- %u > %u\n", payload[n].data, payload[n].left, payload[n].rite);
			int retV = sludgy(payload[n].data);
			if(retV < 0) {
				printf("Fail to sludge.\n");
			}
			updatePointers(&payload, &numMoles, payload[n].data);
		}
	}


	printf("Air cleaning\n");
	for(size_t n = 0; n < numMoles; n++)
	{
		if(payload[n].data == 0)
		{
			// Found air
			printf("Found air molecule.%u\n",payload[n].data );
			updatePointers(&payload, &numMoles, payload[n].data);
		}
	}

	// Phosphate cleaning
	printf("Phosphate cleaning\n");
	size_t prev = 0;
	size_t curr = 0;
	size_t nJumps = 0;
	size_t idx = 0;
	uint32_t* pastMoles = malloc( sizeof(*pastMoles) * numMoles );
	for(size_t n = 0; n < numMoles; n++) {
		// printf("%zu -> ", n);
		memset(pastMoles, '\0', numMoles);
		nJumps = 0;
		idx = 0;
		pastMoles[idx] = n;
		curr = n;
		if( (payload[n].left == 0) && (payload[n].rite != 0))
		{
			prev = curr;
			curr = payload[n].rite;
		}
		else if( (payload[n].left != 0) && (payload[n].rite == 0))
		{
			prev = curr;
			curr = payload[n].left;
		}
		else
		{
			// not a phosphate head
			// printf("\n");
			continue;
		}
		while(nJumps < numMoles)
		{
			idx++;
			pastMoles[idx] = curr;
			// printf("%zu -> ", curr);
			if( (payload[curr].left == 0) && (payload[curr].rite == prev))
			{
				// found tail - found phosphate molecule
				// printf("Found tail.\n");
				handle_phosphates(payload, pastMoles, idx);
				break;
			}
			else if( (payload[n].left == prev) && (payload[n].rite == 0))
			{
				// found tail - found phosphate molecule
				// printf("Found tail.\n");
				handle_phosphates(payload, pastMoles, idx);
				break;
			}
			else if( payload[curr].left == prev) {
				// middle molecule
				prev = curr;
				curr = payload[curr].rite;
			}
			else if( payload[curr].rite == prev) {
				// middle molecule
				prev = curr;
				curr = payload[curr].left;
			}
			else {
				// not a phosphate molecule
				break;
			}

			nJumps++;
		}

		// printf("\n");
	}
	free(pastMoles);

	incGlobals(3, numMoles);

	// Chlorine cleaning
	printf("Chlorine cleaning\n");
	for(size_t n = 0; n < numMoles; n++) {
		if(payload[n].left == 0) {
			continue;
		}
		if(payload[n].left == payload[n].rite) {
			// Chlorine found!
			printf("Found chlorine! mole#:%u -- %u > %u\n", payload[n].data, payload[n].left, payload[n].rite);
			double chlDiff = chlNum / totalMoles;
			if(chlDiff < 0.03) {
				printf("About to increment.\n");
				incGlobals(2, 1);
				printf("Incremented.\n");
				continue;
			}
			if(chlDiff > 0.05) {
				payload[n].rite = 0;
			}
		}
		printf("Loops.\n");
	}

	printf("Cleaning complete.\n");

	for(size_t n = 0; n < numMoles; n++)
	{
		payload[n].data = htonl(payload[n].data);
		payload[n].left = htons(payload[n].left);
		payload[n].rite = htons(payload[n].rite);
	}


	int owater = connect_outgoing(DOWNSTREAM, DOWNSTREAM_PORT);
	if(owater < 0) {
		perror("Could not connect downstream");
		goto done;
	}

	int addAir = 0;
	double airDiff = (double)airNum / totalMoles;
	while(airDiff < 0.01)
	{
		printf("Adding air.\n");
		incGlobals(1, 1);
		incGlobals(3, 1);
		numMoles+=1;
		addAir += 1;
		airDiff = (double)airNum / totalMoles;
	}

	head.size = sizeof(head)+(numMoles * sizeof(*payload));
	printf("Writing %zu molecules\n", numMoles);
	head.type = 0;
	head.size = htons(head.size);
	head.type = htons(head.type);

	sendPayload(owater, &head, payload, numMoles);

	// send(owater, &head, sizeof(head), 0);
	// printf("Sent head.\n" );
	// send(owater, payload, sizeof(*payload) * numMoles, 0);
	// printf("Sent payload.\n" );
	// while(addAir-- > 0) {
	// 	send(owater, &air, sizeof(air), 0);
	// 	printf("%s\n", "Sending air.");
	// }

	close(owater);

done:
	free(payload);
	close(new_stream);
	return(NULL);
}

int sendPayload(int output, struct header* head, void* payload, size_t numMoles)
{
	printf("Sending payload.\n");
	char* toSend = calloc((numMoles + 1) * 8, 1);
	if(!toSend) {
		fprintf(stderr, "Error mallocing.\n");
		return(1);
	}

	memcpy(toSend, head, sizeof(*head));

	memcpy(toSend+sizeof(*head), payload, 8 * numMoles);

	send(output, toSend, sizeof(*head) +(8 * numMoles), 0);

	free(toSend);

	printf("Sent payload.\n");
	return(0);
}


int isFib(uint32_t data)
{
	switch(data)
	{
		case 1:
		case 2:
		case 3:
		case 5:
		case 8:
		case 13:
		case 21:
		case 34:
		case 55:
		case 89:
		case 144:
		case 233:
		case 377:
		case 610:
		case 987:
		case 1597:
		case 2584:
		case 4181:
		case 6765:
		case 10946:
		case 17711:
		case 28657:
		case 46368:
		case 75025:
		case 121393:
		case 196418:
		case 317811:
		case 514229:
		case 832040:
		case 1346269:
		case 2178309:
		case 3524578:
		case 5702887:
		case 9227465:
		case 14930352:
		case 24157817:
		case 39088169:
		case 63245986:
		case 102334155:
		case 165580141:
		case 267914296:
		case 433494437:
		case 701408733:
		case 1134903170:
		case 1836311903:
		case 2971215073:
			return(1);

		default: return(0);
	}

	return(0);
}

void printPayload(struct node* payload, size_t numMoles)
{
	char* tempP = (char*)payload;
	for(size_t i = 0; i < (numMoles)*8; i++)
	{
		if(i % 8 == 0) {
			printf(" ");
		}
		if(i % 16 == 0) {
			printf("\n");
		}
		printf("%02x ", tempP[i]);
	}
	printf("\n");
}


int handle_phosphates(struct node* payload, uint32_t* pastMoles, size_t idx)
{
	if(pastMoles[0] < pastMoles[idx+1]) {
		for(size_t i = idx-1; i > 0;i--)
		{
			payload[pastMoles[i]].left = pastMoles[i+1];
			payload[pastMoles[i]].rite = pastMoles[i+1];
			// printf("%zu -> ", pastMoles[i]);
		}
		payload[pastMoles[0]].left = 0;
		payload[pastMoles[0]].rite = 0;
	}
	else {
		for(size_t i = 0; i < idx;i++)
		{
			payload[pastMoles[i]].left = pastMoles[i+1];
			payload[pastMoles[i]].rite = pastMoles[i+1];
			// printf("%zu -> ", pastMoles[i]);
		}
		payload[pastMoles[idx]].left = 0;
		payload[pastMoles[idx]].rite = 0;
	}

	printf("Treated phosphate!\n");
	return(0);
}


int handle_selenium(struct node* payload, uint32_t* prevMoles, size_t jumps, size_t* numMoles)
{
	uint32_t max = 0;
	for(size_t i = 0; i < jumps; i++)
	{
		if(payload[prevMoles[i]].data > max)
		{
			// max = payload[prevMoles[i]].data;
			max = i;
		}
	}

	hazzy(payload[max].data);

	size_t n = 1;
	for( ; n < *numMoles; n++)
	{
		if(payload[n].left == max)
		{
			payload[n].left = 0;
			payload[n].rite = 0;
			break;
		}
	}

	for(size_t i = 0; i < jumps; i++)
	{
		payload[prevMoles[i]].rite = 0;
	}


	printf("Removing selenium molecule: %u\n", payload[max].data);
	updatePointers(&payload, numMoles, payload[max].data);
	return(0);
}



int main(void)
{
	printf("Initiating locks.\n");
	if (pthread_mutex_init(&airLock, NULL) != 0) {
		printf("Air mutex init failed.\n");
		return(1);
	}
	if (pthread_mutex_init(&chlLock, NULL) != 0) {
		printf("Air mutex init failed.\n");
		return(1);
	}
	if (pthread_mutex_init(&totLock, NULL) != 0) {
		printf("Air mutex init failed.\n");
		return(1);
	}
	printf("Initiation complete.\n");

	int isludge = listen_incoming("1111");
	if(isludge < 0) {
		perror("Could not listen upstream");
		return(-1);
	}

	int new_stream = 0;
	int* sock_thread = NULL;
	while((new_stream = accept_incoming(isludge)))
	{
		if (new_stream < 0) {
			perror("accept failed");
			return(1);
		}
		printf("Conncetion accepted.\n");

		pthread_t sniff_thread;
		sock_thread = malloc(sizeof(*sock_thread));
		*sock_thread = new_stream;

		if (pthread_create( &sniff_thread , NULL, handler, (void*) sock_thread) < 0) {
			perror("could not create thread");
			return(1);
		}

		pthread_join(sniff_thread, NULL);

		printf("Connection closed.\n");
		free(sock_thread);
	}
}
