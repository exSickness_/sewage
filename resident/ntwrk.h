
#ifndef NETWORKING_H
 #define NETWORKING_H


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

extern const char *salt;

struct node {
	uint32_t data;
	uint16_t left;
	uint16_t rite;
};

struct header {
	uint16_t type;
	uint16_t size;
	char custom[4];
};

struct hash {
	uint8_t code[64];
};

struct haz_container {
	uint32_t data;
	uint32_t custom;
};

struct sludge {
	struct header *header;
	struct hash *hashes;
};

struct liquid {
	struct header *header;
	struct node *nodes;
};

int listen_incoming(const char *port);
int accept_incoming(int sd);

int connect_outgoing(const char *host, const char *port);

#endif
